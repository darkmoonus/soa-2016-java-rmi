package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Service extends Remote {
    Book getBook(String id) throws RemoteException;
    String getId(String id) throws RemoteException;
    String getTitle(String id) throws RemoteException;
    String getAuthor(String id) throws RemoteException;
}
