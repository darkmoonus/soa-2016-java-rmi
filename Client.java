package RMI;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
    private static final String HOST = "localhost";
    private static final int PORT = 1111;
    private static Registry registry;

    public static void main(String[] args) throws Exception {
        registry = LocateRegistry.getRegistry(HOST, PORT);
        Service service = (Service) registry.lookup("Demo");

        System.out.println(service.getBook("2").toString());
        System.out.println(service.getId("1"));
        System.out.println(service.getTitle("1"));
        System.out.println(service.getAuthor("1"));

    }
}
