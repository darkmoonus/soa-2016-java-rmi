package RMI;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class BookService extends UnicastRemoteObject implements Service {
    private static final long serialVersionUID = 1L;
    private static HashMap<String, Book> bookMaps = new HashMap<>();

    static {
        bookMaps.put("1", new Book("1", "1", 1.0, new Author("1", 1)));
        bookMaps.put("2", new Book("2", "2", 2.0, new Author("2", 2)));
        bookMaps.put("3", new Book("3", "3", 3.0, new Author("3", 3)));

    }
    public BookService() throws RemoteException {
        super();
    }

    @Override
    public synchronized Book getBook(String id) {
        return bookMaps.get(id);
    }

    @Override
    public synchronized String getId(String id) {
        return bookMaps.get(id).getId();
    }

    @Override
    public synchronized String getTitle(String id) {
        return bookMaps.get(id).getTitle();
    }

    @Override
    public synchronized String getAuthor(String id) {
        return bookMaps.get(id).getAuthor().toString();
    }
}
